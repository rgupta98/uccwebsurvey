using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSurvey.Data;
using WebSurvey.Utils;
using Microsoft.EntityFrameworkCore;

namespace WebSurvey.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  public class LoginController : Controller
  {

    private readonly UCCCContext _context;

    public LoginController(UCCCContext context)
    {
      _context = context;
    }

    [HttpGet]
    [Route("getSession")] //  
    public bool SessionExists()
    {
      var customer = HttpContext.Session.Get<Customer>("UsersCustomer");
      if (customer != null)
        return true;
      return false;
    }

    // GET api/login/5
    [HttpPost()]
    public IActionResult Post([FromBody]CustUser user1)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      var user = _context.CustUsers.Include(x => x.Customer).ThenInclude(x => x.WaterProvider).FirstOrDefault(x => x.CustomerId == user1.CustomerId && x.Email == user1.Email);

      var hash = HashPassword.GetHash(user1.Password);
      if (user != null && user.Password == hash)
      {
        if(user.Customer != null)
        {
          user.Customer.ServiceAddress = user.Customer.GetServiceAddress();
          user.Customer.BillingAddress = user.Customer.GetBillingAddress();
        }
        HttpContext.Session.Set<Customer>("UsersCustomer", user.Customer);
        HttpContext.Session.Set<WaterProvider>("UsersWaterProvider", user.Customer.WaterProvider);
        return new StatusCodeResult(StatusCodes.Status200OK);
      }
      else
      {
        return new StatusCodeResult(StatusCodes.Status203NonAuthoritative);
      }
    }

  }
}
