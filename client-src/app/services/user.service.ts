import { Injectable } from '@angular/core';
import { AsyncLocalStorage } from 'angular-async-local-storage';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class UserService {

  private isUserLoggedIn: boolean;
  public username: string;
  public sessionExists: boolean;

  constructor(public http: Http, protected localStorage: AsyncLocalStorage) {
    this.isUserLoggedIn = false;
   }

  setUserLoggedIn() {
      this.isUserLoggedIn = true;
      return this.localStorage.setItem('loggedIn', this.isUserLoggedIn);
    }


  resetLocalStorage() {
    this.isUserLoggedIn = false;
    this.localStorage.removeItem('loggedIn').subscribe(() => { });
    this.localStorage.removeItem('email').subscribe(() => { });
    this.localStorage.removeItem('password').subscribe(() => { });
  }
  getUserLoggedIn() {

    return Observable.create(observer => {
      this.http.get("api/Login/getSession").subscribe(data => {
        this.sessionExists = data.json();
        if (!this.sessionExists) {
          this.resetLocalStorage();
          observer.next(this.sessionExists);
          observer.complete();
        }
        else {
          this.localStorage.getItem<string>('loggedIn').subscribe(v => {
            this.isUserLoggedIn = v;
            observer.next(v);
            observer.complete();
          })
        }
      });
    });

  }
}
