using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSurvey.Data;
using WebSurvey.Utils;

namespace WebSurvey.Controllers
{
  [Produces("application/json")]
  [Route("api/Survey")]
  public class SurveyController : Controller
  {
    private readonly UCCCContext _context;
    public SurveyController(UCCCContext context)
    {
      _context = context;
    }

    [HttpPost()]
    [Route("addSurvey")] //
    public async Task<IActionResult> Post([FromBody]CustSurvey survey)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      survey.CCId = GenerateCCId();

      if (SurveyExists(survey.CCId))
      {
        return new StatusCodeResult(StatusCodes.Status409Conflict);
      }

      if(survey.ConnectedPlants != null)
      {
        foreach (var item in survey.ConnectedPlants)
        {
          item.CustSurvey = survey;
        }
      }
      if(survey.WaterReceivers != null)
      {
        foreach (var item in survey.WaterReceivers)
        {
          item.CustSurvey = survey;
        }
      }

      _context.CustSurveys.Add(survey);
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateException)
      {
          throw;
      }

      return CreatedAtAction("GetSurvey", new { id = survey.CCId }, survey);
    }

    private bool SurveyExists(string ccId)
    {
      return _context.CustSurveys.Any(e => e.CCId == ccId);
    }
    private string GenerateCCId()
    {
      var cust = HttpContext.Session.Get<Customer>("UsersCustomer");
      if (cust != null && cust.WaterProvider != null)
      {
        var otherCustomers = _context.Customers.Where(x => x.AccountId == cust.AccountId).OrderBy(x => x.WaterMeterNumber).ToList();
        int cnt = 1;
        for (int i = 0; i < otherCustomers.Count; i++)
        {
          if(otherCustomers[i].WaterMeterNumber == cust.WaterMeterNumber)
          {
            cnt = i + 1;
            break;
          }
        }
        return $"{cust.WaterProvider.ProviderName}-{cust.AccountId}-{cnt}";
      }
      return string.Empty;
    }

  }
}
