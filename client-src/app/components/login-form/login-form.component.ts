import { Component, OnInit, Input, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { UserService } from '../../services/user.service';
import { AsyncLocalStorage } from 'angular-async-local-storage';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css', '../../../assets/css/index.css', '../../../assets/css/login.css']
})
export class LoginFormComponent implements OnInit {

  public email: string = '';
  public password: string = '';
  public rememberMe: boolean = false;
  public bseUrl: string = "";
  public invalidLogin: boolean = false;
  constructor(private router: Router, private user: UserService, protected localStorage: AsyncLocalStorage, public http: Http, @Inject('BASE_URL') baseUrl: string) {
    this.bseUrl = baseUrl; 
  }

  ngOnInit() {
  }

  performLogin() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    this.http.post(this.bseUrl + 'api/Login/', JSON.stringify({ CustomerId: 1, Email: this.email, Password: this.password }),
      { headers: headers }).subscribe(
      r => {
        if (r.status == 200) {
          this.invalidLogin = false;
          this.localStorage.setItem('email', this.email).subscribe(() => { });
          this.localStorage.setItem('password', this.password).subscribe(() => { });
          this.user.setUserLoggedIn().subscribe(x => {
            this.router.navigate(['home']);
          })
        }
        else {
          this.invalidLogin = true;
        }
      }, error => {
      }
      );
  }

loginUser(e:any) {
    e.preventDefault();

  this.performLogin();
}
}

export interface CustUser {
  Id: number;
  CustomerId: number;
  email: string;
  password: string;
} 
