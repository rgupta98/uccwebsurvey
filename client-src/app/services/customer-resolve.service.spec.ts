import { TestBed, inject } from '@angular/core/testing';

import { CustomerResolveService } from './customer-resolve.service';

describe('CustomerResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerResolveService]
    });
  });

  it('should be created', inject([CustomerResolveService], (service: CustomerResolveService) => {
    expect(service).toBeTruthy();
  }));
});
