using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebSurvey.Data;
using WebSurvey.Utils;

namespace WebSurvey.Controllers
{
  [Produces("application/json")]
  [Route("api/Provider")]
  public class ProviderController : Controller
  {
    private readonly UCCCContext _context;
    public ProviderController(UCCCContext context)
    {
      _context = context;
    }

    [HttpGet]
    [Route("getImage/{customerId}")] //  
    public string GetImage(int customerId)
    {
      var provider = HttpContext.Session.Get<WaterProvider>("UsersWaterProvider");
      if(provider != null)
        return System.Convert.ToBase64String(provider.Logo);
      else
      {
        var logo = _context.Customers.Include(x => x.WaterProvider).FirstOrDefault(x => x.CustomerId == customerId).WaterProvider.Logo;
        return System.Convert.ToBase64String(logo);
      }
    }

    [HttpGet]
    [Route("getCustomer/{customerId}")] //
    public Customer GetCust(int customerId)
    {
      var customer = HttpContext.Session.Get<Customer>("UsersCustomer");
      if (customer != null)
        return customer;
      else
      {
        var cust = _context.Customers.Include(x => x.WaterProvider).FirstOrDefault(x => x.CustomerId == customerId);
        return cust;
      }
    }
  }
}
