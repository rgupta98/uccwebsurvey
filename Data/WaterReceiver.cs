using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebSurvey.Data
{
  public class WaterReceiver
  {
    [Key]
    public virtual int WaterReceiverId { get; set; }
    public virtual int CustSurveyId { get; set; }
    public virtual CustSurvey CustSurvey { get; set; }
    public virtual string SupplyReceiver { get; set; }
  }
}
