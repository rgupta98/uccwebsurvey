using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSurvey.Data
{
  public class WaterProvider
  {
    public virtual int WaterProviderId { get; set; }
    public virtual string ProviderName { get; set; }
    public virtual byte[] Logo { get; set; }

    [JsonIgnore]
    public virtual IList<Customer> Customers { get; set; }
  }
}
