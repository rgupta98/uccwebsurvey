using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSurvey.Data
{
  public class UCCCContext : DbContext
  {
    public UCCCContext(DbContextOptions<UCCCContext> options)
        : base(options) { }
    public UCCCContext() { }
    public DbSet<Customer> Customers { get; set; }
    public DbSet<WaterProvider> WaterProviders { get; set; }
    public DbSet<CustUser> CustUsers { get; set; }
    public DbSet<CustSurvey> CustSurveys { get; set; }
    public DbSet<ConnectedPlant> ConnectedPlants { get; set; }
    public DbSet<WaterReceiver> WaterReceivers { get; set; }
  }
}
