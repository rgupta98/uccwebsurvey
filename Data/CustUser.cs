using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebSurvey.Data
{
  public class CustUser
  {
    public virtual Customer Customer { get; set; }
    [Key]
    public virtual int CustUserId { get; set; }
    public virtual int CustomerId { get; set; }
    public virtual string Email { get; set; }

    public virtual string Password { get; set; }
  }
}
