import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AlertModule } from 'ngx-bootstrap';
import { AsyncLocalStorageModule } from 'angular-async-local-storage';
import { TextMaskModule } from 'angular2-text-mask';
import { NgDatepickerModule } from 'ng2-datepicker';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from './services/auth.guard';
import { UserService } from './services/user.service'
import { CustomerResolve } from './services/customer-resolve.service'
import { HeaderComponent } from './components/header/header.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { FooterComponent } from './components/footer/footer.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { ProviderImageComponent } from './components/provider-image/provider-image.component';

const appRoutes: Routes = [
  {
    path: 'home',
    canActivate: [AuthGuard],
    component: HomeComponent,
    resolve: {
      customer: CustomerResolve
    }
  },
  {
    path: '',
    pathMatch: 'prefix',
    component: LoginFormComponent
  },
  {
    path: '**',
    component: NotfoundComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HeaderComponent,
    LoginFormComponent,
    FooterComponent,
    NotfoundComponent,
    HomeComponent,
    ProviderImageComponent
  ],
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    AlertModule.forRoot(),
    AsyncLocalStorageModule,
    TextMaskModule,
    NgDatepickerModule,
    BrowserAnimationsModule,
    ToastModule.forRoot()
  ],
  providers: [UserService, AuthGuard, CustomerResolve, { provide: 'BASE_URL', useFactory: getBaseUrl }],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}
