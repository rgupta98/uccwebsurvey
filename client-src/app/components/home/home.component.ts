import { Component, OnInit, Input, Inject, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ProviderImageComponent } from "../provider-image/provider-image.component"
import { NgDatepickerComponent } from "ng2-datepicker"
import { DatepickerOptions } from "ng2-datepicker"
import * as enLocale from 'date-fns/locale/en';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['../../../assets/css/index.css']
})
export class HomeComponent implements OnInit {
  public bseUrl: string = "";
  public surveyAlreadyExists: boolean = false;
  options: DatepickerOptions = {
    locale: enLocale
  };
  public otherWaterUsingEquipmentDesc: string = "";
  public otherPropertyTypeValue: string = "";
  public contactEmailAddress: string = "";
  public otherConnectedPlant: string = "";
  public connectedPlants: string[] = new Array();
  public arrConnectedPlants: ConnectedPlant[] = new Array();

  public waterReceivers: string[] = new Array();
  public arrWaterReceivers: WaterReceiver[] = new Array();

  public surveyOwnerFullName: string = "";
  public surveyDate: Date;
  public surveyOwnerPhone: string = "";

  public Customer: Customer = null;
  public residentialPropType: boolean = false;
  public commercialPropType: boolean = false;
  public otherPropType: boolean = false;

  public otherEquipmentYes: boolean = false;
  public otherEquipmentNo: boolean = false;
  public otherEquipmentUnsure: boolean = false;

  public contactByEmailYes: boolean = false;
  public contactByEmailNo: boolean = false;
  public backflowPrevDeviceUnsure: boolean = false;
  public backflowPrevDeviceNo: boolean = false;
  public backflowPrevDeviceYes: boolean = false;
  mask: any[] = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(private router: Router, public http: Http, @Inject('BASE_URL') baseUrl: string, private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.bseUrl = baseUrl;
    this.surveyDate = new Date();
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.Customer = this.route.snapshot.data['customer'];
    for (var i = 0; i < 28; i++) {
      this.connectedPlants[i] = "";
      this.waterReceivers[i] = "";
    }
  }

  updateConnectedPlants($event: any) {
    if ($event.target.checked)
      this.connectedPlants[$event.target.getAttribute('idx')] = $event.target.getAttribute('value');
    else
      this.connectedPlants[$event.target.getAttribute('idx')] = "";
  }

  updateWaterReceivers($event: any) {
    if ($event.target.checked)
      this.waterReceivers[$event.target.getAttribute('idx')] = $event.target.getAttribute('value');
    else
      this.waterReceivers[$event.target.getAttribute('idx')] = "";
  }

  updateContactByEmail(e: any) {
    if (e == "yes") {
      this.contactByEmailYes = true;
      this.contactByEmailNo = false;
    }
    if (e == "no") {
      this.contactByEmailNo = true;
      this.contactByEmailYes = false;
    }
  }
  updateBackflowDevice(e: any) {
    if (e == "yes") {
      this.backflowPrevDeviceYes = true;
      this.backflowPrevDeviceNo = false;
      this.backflowPrevDeviceUnsure = false;
    }
    if (e == "no") {
      this.backflowPrevDeviceNo = true;
      this.backflowPrevDeviceYes = false;
      this.backflowPrevDeviceUnsure = false;
    }
    if (e == "unsure") {
      this.backflowPrevDeviceUnsure = true;
      this.backflowPrevDeviceYes = false;
      this.backflowPrevDeviceNo = false;
    }
  }

  updateOtherEquipment(e: any) {
    if (e == "yes") {
      this.otherEquipmentYes = true;
      this.otherEquipmentNo = false;
      this.otherEquipmentUnsure = false;
    }
    if (e == "no") {
      this.otherEquipmentNo = true;
      this.otherEquipmentYes = false;
      this.otherEquipmentUnsure = false;
    }
    if (e == "unsure") {
      this.otherEquipmentUnsure = true;
      this.otherEquipmentYes = false;
      this.otherEquipmentNo = false;
    }
  }

  updatePropertyType(e: any) {
    if (e == "residential") {
      this.residentialPropType = true;
      this.commercialPropType = false;
      this.otherPropType = false;
    }
    if (e == "commercial") {
      this.commercialPropType = true;
      this.residentialPropType = false;
      this.otherPropType = false;
    }
    if (e == "other") {
      this.otherPropType = true;
      this.commercialPropType = false;
      this.residentialPropType = false;
    }
  }

  onSubmit(e: any) {
    var propType: string = "residential";
    if (this.residentialPropType)
      propType = "residential";
    else if (this.commercialPropType)
      propType = "commercial";
    else if (this.otherPropType)
      propType = "other";

    var otherEquipment = "yes";
    if (this.otherEquipmentYes)
      otherEquipment = "yes";
    else if (this.otherEquipmentNo)
      otherEquipment = "no";
    else if (this.otherEquipmentUnsure)
      otherEquipment = "unsure";

    var backflow = "yes";
    if (this.backflowPrevDeviceYes)
      backflow = "yes";
    else if (this.backflowPrevDeviceNo)
      backflow = "no";
    else if (this.backflowPrevDeviceUnsure)
      backflow = "unsure";

    this.arrConnectedPlants = new Array();

    for (var i = 0; i < 28; i++) {
      var val = this.connectedPlants[i];
      if (val != undefined && val.length > 1) {
        var p: ConnectedPlant = {
          connectedPlantName: val
        };
        this.arrConnectedPlants.push(p);
      }
    }
    if (this.otherConnectedPlant != undefined && this.otherConnectedPlant.length > 1) {
      var p: ConnectedPlant = {
        connectedPlantName: this.otherConnectedPlant
      };
      this.arrConnectedPlants.push(p);
    }

    this.arrWaterReceivers = new Array();

    for (var i = 0; i < 28; i++) {
      var val = this.waterReceivers[i];
      if (val != undefined && val.length > 1) {
        var p1: WaterReceiver = {
          supplyReceiver: val
        };
        this.arrWaterReceivers.push(p1);
      }
    }

    var model: CustSurvey = {
      customerId: this.Customer.customerId,
      cCId: "test",
      serviceClass: this.Customer.serviceClass,
      diameter: this.Customer.diameter,
      propertyType: propType,
      propertyTypeValue: this.otherPropertyTypeValue,
      otherWaterUsingEquipment: otherEquipment,
      otherWaterUsingEquipmentDesc: this.otherWaterUsingEquipmentDesc,
      backflowPreventionDevice: backflow,
      surveyDate: this.surveyDate,
      surveyOwnerFullName: this.surveyOwnerFullName,
      surveyOwnerPhone: this.surveyOwnerPhone,
      contactEmailAddress: this.contactEmailAddress,
      connectedPlants: this.arrConnectedPlants,
      waterReceivers: this.arrWaterReceivers
    }

    var headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');

    this.http.post(this.bseUrl + 'api/Survey/addSurvey', JSON.stringify(model),
      { headers: headers }).subscribe(
      response => {
        var r = response.json();
        this.toastr.success("Customer survey was submitted successfully with CCID :" + r.ccId, "Survey created successfully");
      }, error => {
        if (error.status == 409) {
          this.toastr.error('Survey for the Meter already exists!', 'Survey Already Exists!');
          //window.scrollTo(0, 100);
        }
      }
      );

  }
}

export interface Customer {
  customerId: number;
  accountId: number;
  waterMeterNumber: number;
  cCId: string;
  serviceClass: string;
  diameter: number;
}

export interface CustSurvey {
  customerId: number;
  cCId: string;
  serviceClass: string;
  diameter: number;
  propertyType: string;
  propertyTypeValue: string;
  backflowPreventionDevice: string;
  otherWaterUsingEquipment: string;
  otherWaterUsingEquipmentDesc: string;
  surveyDate: Date;
  surveyOwnerPhone: string;
  surveyOwnerFullName: string;
  contactEmailAddress: string;
  connectedPlants: ConnectedPlant[];
  waterReceivers: WaterReceiver[];
}

export interface ConnectedPlant {
  connectedPlantName: string;
}

export interface WaterReceiver {
  supplyReceiver: string;
}

